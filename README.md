### Run

git clone https://gitlab.com/rodosskiand/mtproxy-helm-chart

helm install --name=<name> mtproxy-helm-chart --wait

### Commands
| Command | Result |
| ------ | ------ |
| GET /users  | List users |
| POST /users {"name":"<name>"} | Create user | 
| DELETE /users/<name> | Delete user | 

### Use
for connecting use EXTERNAL-IP OF Load Balancer, secret from response

### example: 
tg://proxy?server=**146.148.37.135**&port=30712&secret=**dd0123456789abcdef0123456789abcdef**


## MOVED